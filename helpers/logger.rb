require 'browser'

def log_request(request)
	browser = Browser.new(:ua => request.user_agent, :accept_language => "en-us")
	if browser.ie6?
		browser_name = "ie6"
	elsif browser.ie7?
		browser_name = "ie7"
	elsif browser.ie8?
		browser.name = "ie8"
	elsif browser.ie9?
		browser.name = "ie9"
	elsif browser.ie10?
		browser.name = "ie10"
	else
		browser_name = browser.name.downcase
	end

	File.open('log/access.log', 'a') do |f|
		f.write(request.path_info + " " + browser_name + "\n")
	end
end