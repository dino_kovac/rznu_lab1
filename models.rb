require 'rubygems'
require 'data_mapper'
require 'bcrypt'

# If you want the logs displayed you have to do this before the call to setup
# DataMapper::Logger.new($stdout, :debug)

# An in-memory Sqlite3 connection:
# DataMapper.setup(:default, 'sqlite::memory:')

# A Sqlite3 connection to a persistent database
# DataMapper.setup(:default, 'sqlite:///db/snippy.db')
DataMapper.setup(:default, "sqlite://#{Dir.pwd}/db.sqlite")

class User
  include DataMapper::Resource
  include BCrypt

  property :id,         Serial,   :key => true
  property :name,      	String,   :length => 3..50
  property :email,   	  String,   :required => true, :unique => true, :format => :email_address,
  :messages => {
      :presence  => "We need your email address.",
      :is_unique => "We already have that email.",
      :format    => "Doesn't look like an email address to me ..."
    }
  property :password,	  BCryptHash
  property :created_at, DateTime
  property :updated_at,	DateTime

  has n, :snippets

  def authenticate(attempted_password)
    if self.password == attempted_password
      true
    else
      false
    end
  end
end

class Snippet
  include DataMapper::Resource

  property :id,         Serial
  property :body,       Text
  property :created_at, DateTime
  property :updated_at,	DateTime

  belongs_to :user
end

DataMapper.finalize

# DataMapper.auto_migrate!

DataMapper.auto_upgrade!