ENV['RACK_ENV'] = 'test'

require './snippy'
require 'test/unit'
require 'rack/test'

class SnippyTest < Test::Unit::TestCase
  include Rack::Test::Methods

  @email = "test123@example.com"
  @password = "test"

  def app
    Snippy
  end

  def test_can_get_users
    get '/users'
    assert last_response.ok?
  end

  def test_can_get_user
    get '/users/4'
    assert last_response.ok?
  end

  def test_cannot_edit_user_unauthorized
    put '/users/4'
    assert last_response.forbidden?
  end

  def test_cannot_delete_user_unauthorized
    delete '/users/4'
    assert last_response.forbidden?
  end

  def test_can_get_snippets
    get '/snippets'
    assert last_response.ok?
  end

  def test_can_get_user_snippets
    get '/users/4/snippets'
    assert last_response.ok?
  end

  def test_can_get_snippet
    get '/snippets/1'
    assert last_response.ok?
  end

  def test_cannot_post_snippet_unauthorized
    post '/snippets'
    assert last_response.forbidden?
  end

  def test_cannot_edit_snippet_unauthorized
    put '/snippets/1'
    assert last_response.forbidden?
  end

  def test_cannot_delete_snippet_unauthorized
    delete '/snippets/1'
    assert last_response.forbidden?
  end

end