require 'rubygems'
require 'bundler/setup'
require 'sinatra/base'
require 'sinatra/contrib'
require 'sinatra/json'
require 'sinatra/respond_with'
require 'sinatra/reloader'
require 'slim'
require 'rack/flash'
require 'warden'
require 'json'
require 'yajl'

# load the database and user models
require './models'

# helpers
require './helpers/logger.rb'

class Snippy < Sinatra::Base
	register Sinatra::Contrib
	register Sinatra::RespondWith

	configure :development do
		puts "Development mode, reloader enabled."
		register Sinatra::Reloader
	end

	enable :method_override # needed for PUT and DELETE

	use Rack::Session::Cookie, secret: "nothingissecretontheinternet"
	use Rack::Flash, accessorize: [:error, :success]

	use Warden::Manager do |config|
		# Tell Warden how to save our User info into a session.
		# Sessions can only take strings, not Ruby code, we'll store
		# the User's `id`
		config.serialize_into_session{|user| user.id }
		# Now tell Warden how to take what we've stored in the session
		# and get a User from that information.
		config.serialize_from_session{|id| User.get(id) }

		config.scope_defaults :default,
			# "strategies" is an array of named methods with which to
			# attempt authentication. We have to define this later.
			strategies: [:password],
			# The action is a route to send the user to when
			# warden.authenticate! returns a false answer. We'll show
			# this route below.
			action: 'login'
		# When a user tries to log in and cannot, this specifies the
		# app to send the user to.
		config.failure_app = self
	end

	Warden::Manager.before_failure do |env,opts|
		env['REQUEST_METHOD'] = 'GET' # POST?
	end

	Warden::Strategies.add(:password) do
		def valid?
			params['user'] && params['user']['email'] && params['user']['password']
		end

		def authenticate!
			user = User.first(email: params['user']['email'])

			if user.nil?
				fail!("The email you entered does not exist.")
				env['x-rack.flash'].error = "The email you entered does not exist"
			elsif user.authenticate(params['user']['password'])
				env['x-rack.flash'].success = "Successfully Logged In"
				success!(user)
			else
				fail!("Could not log in")
			end
		end
	end

	# routes

	before do
		# log it to a file
		log_request(request)

		# allow *.json in path
		if request.path_info =~ /.*.json/
			content_type "application/json"
			request.path_info = request.path_info[0..-6]
		end
	end

	respond_to :html, :json

	get '/' do
		@current_user = env['warden'].user
		slim :home
	end

	get '/login' do
		@current_user = env['warden'].user
		slim :login
	end

	post '/login' do
		env['warden'].authenticate!

	    env['x-rack.flash'].success = env['warden'].message

	    # if session[:return_to].nil?
	    #   redirect '/'
	    # else
	    #   redirect session[:return_to]
	    # end

		@current_user = env['warden'].user
		if @current_user
			redirect "/users/#{@current_user.id}"
	    end
	end

	get '/logout' do
		@current_user = env['warden'].user
		if @current_user
			env['warden'].raw_session.inspect
			env['warden'].logout
			env['x-rack.flash'].success = 'Successfully logged out'
		end
		redirect '/'
	end

	post '/unauthenticated' do
		@current_user = env['warden'].user
		session[:return_to] = env['warden.options'][:attempted_path]
		puts env['warden.options'][:attempted_path]
		env['x-rack.flash'].error = env['warden'].message || "You must log in"
		redirect '/login'
	end

  	get '/users' do
  		@current_user = env['warden'].user
		@users = User.all(:order => [:created_at.desc])
		respond_to do |wants|
			wants.html {
				halt slim :users
			}
			wants.json {
				halt @users.to_json(:exclude => [:password])
			}
		end
	end

	get '/users/new' do
		@current_user = env['warden'].user
		slim :new_user
	end

	post '/users' do
		@current_user = env['warden'].user
		puts params.inspect
		@user = User.create(
			:name => params[:user][:name],
			:email => params[:user][:email],
			:password => params[:user][:password]
			)
		if @user.saved?
			redirect "/users/#{@user.id}"
		else
			slim "h1 We already have a user with that email!"
		end
	end

	get '/users/:id/snippets' do
		@current_user = env['warden'].user
		@user = User.get(params[:id])
		@snippets = Snippet.all(:user => @user, :order => [:updated_at.desc])
		respond_to do |wants|
			wants.html {
				halt slim :snippets
			}
			wants.json {
				halt @snippets.to_json
			}
		end
	end

	get '/users/:id' do
		@current_user = env['warden'].user
		@user = User.get(params[:id])
		if @user
			respond_to do |wants|
				wants.html {
					halt slim :user
				}
				wants.json {
					halt @user.to_json(:exclude => [:password])
				}
			end
		else
			404
		end
	end

	get '/users/:id/edit' do
		@current_user = env['warden'].user
		@user = User.get(params[:id])
		halt 404 unless @user
		slim :edit_user
	end

	put '/users/:id' do
		@current_user = env['warden'].user
		halt 403 unless @current_user
		@user = User.get(params[:id])
		halt 404 unless @user
		halt 403 unless @user.id == @current_user.id

		@user.name = params[:user][:name]
		@user.email = params[:user][:email]
		@user.password = params[:user][:password]
		@user.save

		if @user.saved?
			redirect "/users/#{@user.id}"
		else
			slim "h1 We already have a user with that email!"
		end
	end

	delete '/users/:id' do
		@current_user = env['warden'].user
		halt 403 unless @current_user
		@user = User.get(params[:id])
		halt 404 unless @user
		halt 403 unless @user.id == @current_user.id

		@user.destroy
		redirect '/users'
	end

	get '/snippets/new' do
		@current_user = env['warden'].user
		slim :new_snippet
	end

	post '/snippets' do
		@current_user = env['warden'].user
		halt 403 unless @current_user
		@snippet = Snippet.create(
			:body => params[:snippet][:body],
			:user => @current_user
			)
		if @snippet.saved?
			redirect "/snippets/#{@snippet.id}"
		else
			slim "h1 Could not create snippet!"
		end
	end

	get '/snippets' do
		@current_user = env['warden'].user
		@snippets = Snippet.all(:order => [:updated_at.desc])
		respond_to do |wants|
			wants.html {
				halt slim :snippets
			}
			wants.json {
				halt @snippets.to_json
			}
		end
	end

	get '/snippets/:id' do
		@current_user = env['warden'].user
		@snippet = Snippet.get(params[:id])
		@user = User.get(@snippet.user_id)
		if @snippet
			respond_to do |wants|
				wants.html {
					halt slim :snippet
				}
				wants.json {
					halt @snippet.to_json
				}
			end
		else
			404
		end
	end

	get '/snippets/:id/edit' do
		@current_user = env['warden'].user
		@snippet = Snippet.get(params[:id])
		halt 404 unless @snippet
		slim :edit_snippet
	end

	put '/snippets/:id' do
		@current_user = env['warden'].user
		halt 403 unless @current_user
		@snippet = Snippet.get(params[:id])
		halt 404 unless @snippet
		halt 403 unless @snippet.user == @current_user

		@snippet.body = params[:snippet][:body]
		@snippet.save

		redirect "/snippets/#{@snippet.id}"
	end

	delete '/snippets/:id' do
		@current_user = env['warden'].user
		halt 403 unless @current_user
		@snippet = Snippet.get(params[:id])
		halt 404 unless @current_user
		halt 403 unless @snippet.user == @current_user

		@snippet.destroy

		redirect '/snippets'
	end

	get '/api' do
		@current_user = env['warden'].user
		@routes = [
			[:post, '/login'],
			[:get, '/logout'],
			[:get, '/users'],
			[:post, '/users'],
			[:get, '/users/:id'],
			[:put, '/users/:id'],
			[:delete, '/users/:id'],
			[:get, '/users/:id/snippets'],
			[:get, '/snippets'],
			[:post, '/snippets'],
			[:get, '/snippets/:id'],
			[:put, '/snippets/:id'],
			[:delete, '/snippets/:id']
		]
		slim :api
	end

	error 403 do
		@current_user = env['warden'].user
		slim :error_403
	end

	error 404 do
		@current_user = env['warden'].user
		slim :error_404
	end

	error 500 do
		@current_user = env['warden'].user
		slim :error_500
	end

  	# start the server if ruby file executed directly
  	run! if app_file == $0

end